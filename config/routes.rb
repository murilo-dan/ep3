Rails.application.routes.draw do
  resources :homes
  devise_for :users

  resources :mangas do
    resources :review_mangas, except: [:show, :index]
  end
  resources :animes do
    resources :review_animes, except: [:show, :index]
  end
  resources :books do
    resources :review_books, except: [:show, :index]
  end
  resources :games do
    resources :review_games, except: [:show, :index]
  end
  resources :movies do
    resources :reviews, except: [:show, :index]
  end
  root 'homes#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
