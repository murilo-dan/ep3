class ReviewBooksController < ApplicationController
  before_action :set_review_book, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_book

  # GET /review_books/new
  def new
    @review_book = ReviewBook.new
  end

  # GET /review_books/1/edit
  def edit
  end

  # POST /review_books
  # POST /review_books.json
  def create
    @review_book = ReviewBook.new(review_book_params)
    @review_book.user_id = current_user.id
    @review_book.book_id = @book.id

    respond_to do |format|
      if @review_book.save
        format.html { redirect_to @book, notice: 'Análise feita com sucesso.' }
        format.json { render :show, status: :created, location: @review_book }
      else
        format.html { render :new }
        format.json { render json: @review_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /review_books/1
  # PATCH/PUT /review_books/1.json
  def update
    respond_to do |format|
      if @review_book.update(review_book_params)
        format.html { redirect_to @book, notice: 'Análise editada com sucesso.' }
        format.json { render :show, status: :ok, location: @review_book }
      else
        format.html { render :edit }
        format.json { render json: @review_book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /review_books/1
  # DELETE /review_books/1.json
  def destroy
    @review_book = ReviewBook.find(params[:id])
    @review_book.destroy!
    redirect_to @book, notice: 'Análise deletada com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review_book
      @review_book = ReviewBook.find(params[:id])
    end

    def set_book
      @book = Book.find(params[:book_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_book_params
      params.require(:review_book).permit(:rating, :comment)
    end
end
