class ReviewAnimesController < ApplicationController
  before_action :set_review_anime, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_anime

  # GET /review_animes/new
  def new
    @review_anime = ReviewAnime.new
  end

  # GET /review_animes/1/edit
  def edit
  end

  # POST /review_animes
  # POST /review_animes.json
  def create
    @review_anime = ReviewAnime.new(review_anime_params)
    @review_anime.user_id = current_user.id
    @review_anime.anime_id = @anime.id

    respond_to do |format|
      if @review_anime.save
        format.html { redirect_to @anime, notice: 'Análise feita com sucesso.' }
        format.json { render :show, status: :created, location: @review_anime }
      else
        format.html { render :new }
        format.json { render json: @review_anime.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /review_animes/1
  # PATCH/PUT /review_animes/1.json
  def update
    respond_to do |format|
      if @review_anime.update(review_anime_params)
        format.html { redirect_to @anime, notice: 'Análise editada com sucesso.' }
        format.json { render :show, status: :ok, location: @review_anime }
      else
        format.html { render :edit }
        format.json { render json: @review_anime.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /review_animes/1
  # DELETE /review_animes/1.json
  def destroy
    @review_anime = ReviewAnime.find(params[:id])
    @review_anime.destroy!
    redirect_to @anime, notice: 'Análise deletada com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review_anime
      @review_anime = ReviewAnime.find(params[:id])
    end

    def set_anime
      @anime = Anime.find(params[:anime_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_anime_params
      params.require(:review_anime).permit(:rating, :comment)
    end
end
