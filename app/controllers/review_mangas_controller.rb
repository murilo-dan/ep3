class ReviewMangasController < ApplicationController
  before_action :set_review_manga, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_manga

  # GET /review_mangas/new
  def new
    @review_manga = ReviewManga.new
  end

  # GET /review_mangas/1/edit
  def edit
  end

  # POST /review_mangas
  # POST /review_mangas.json
  def create
    @review_manga = ReviewManga.new(review_manga_params)
    @review_manga.user_id = current_user.id
    @review_manga.manga_id = @manga.id

    respond_to do |format|
      if @review_manga.save
        format.html { redirect_to @manga, notice: 'Análise feita com sucesso.' }
        format.json { render :show, status: :created, location: @review_manga }
      else
        format.html { render :new }
        format.json { render json: @review_manga.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /review_mangas/1
  # PATCH/PUT /review_mangas/1.json
  def update
    respond_to do |format|
      if @review_manga.update(review_manga_params)
        format.html { redirect_to @manga, notice: 'Análise editada com sucesso.' }
        format.json { render :show, status: :ok, location: @review_manga }
      else
        format.html { render :edit }
        format.json { render json: @review_manga.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /review_mangas/1
  # DELETE /review_mangas/1.json
  def destroy
    @review_manga = ReviewManga.find(params[:id])
    @review_manga.destroy!
    redirect_to @manga, notice: 'Análise deletada com sucesso.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review_manga
      @review_manga = ReviewManga.find(params[:id])
    end

    def set_manga
      @manga = Manga.find(params[:manga_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_manga_params
      params.require(:review_manga).permit(:rating, :comment)
    end
end
