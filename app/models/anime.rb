class Anime < ApplicationRecord
  has_one_attached :image
  has_many :review_animes
end
