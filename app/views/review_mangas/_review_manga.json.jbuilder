json.extract! review_manga, :id, :rating, :comment, :created_at, :updated_at
json.url review_manga_url(review_manga, format: :json)
