json.extract! game, :id, :title, :description, :rating, :developer, :genre, :publisher, :release_date, :platform, :created_at, :updated_at
json.url game_url(game, format: :json)
