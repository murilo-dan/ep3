json.extract! anime, :id, :title, :synopsis, :rating, :genre, :prequel, :sequel, :created_at, :updated_at
json.url anime_url(anime, format: :json)
