json.extract! review_game, :id, :rating, :comment, :created_at, :updated_at
json.url review_game_url(review_game, format: :json)
