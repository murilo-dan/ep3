json.extract! manga, :id, :title, :synopsis, :author, :created_at, :updated_at
json.url manga_url(manga, format: :json)
