json.extract! review_anime, :id, :rating, :comment, :created_at, :updated_at
json.url review_anime_url(review_anime, format: :json)
