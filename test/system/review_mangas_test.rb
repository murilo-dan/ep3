require "application_system_test_case"

class ReviewMangasTest < ApplicationSystemTestCase
  setup do
    @review_manga = review_mangas(:one)
  end

  test "visiting the index" do
    visit review_mangas_url
    assert_selector "h1", text: "Review Mangas"
  end

  test "creating a Review manga" do
    visit review_mangas_url
    click_on "New Review Manga"

    fill_in "Comment", with: @review_manga.comment
    fill_in "Rating", with: @review_manga.rating
    click_on "Create Review manga"

    assert_text "Review manga was successfully created"
    click_on "Back"
  end

  test "updating a Review manga" do
    visit review_mangas_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @review_manga.comment
    fill_in "Rating", with: @review_manga.rating
    click_on "Update Review manga"

    assert_text "Review manga was successfully updated"
    click_on "Back"
  end

  test "destroying a Review manga" do
    visit review_mangas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Review manga was successfully destroyed"
  end
end
