require "application_system_test_case"

class ReviewBooksTest < ApplicationSystemTestCase
  setup do
    @review_book = review_books(:one)
  end

  test "visiting the index" do
    visit review_books_url
    assert_selector "h1", text: "Review Books"
  end

  test "creating a Review book" do
    visit review_books_url
    click_on "New Review Book"

    fill_in "Comment", with: @review_book.comment
    fill_in "Rating", with: @review_book.rating
    click_on "Create Review book"

    assert_text "Review book was successfully created"
    click_on "Back"
  end

  test "updating a Review book" do
    visit review_books_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @review_book.comment
    fill_in "Rating", with: @review_book.rating
    click_on "Update Review book"

    assert_text "Review book was successfully updated"
    click_on "Back"
  end

  test "destroying a Review book" do
    visit review_books_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Review book was successfully destroyed"
  end
end
