require 'test_helper'

class ReviewAnimesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review_anime = review_animes(:one)
  end

  test "should get index" do
    get review_animes_url
    assert_response :success
  end

  test "should get new" do
    get new_review_anime_url
    assert_response :success
  end

  test "should create review_anime" do
    assert_difference('ReviewAnime.count') do
      post review_animes_url, params: { review_anime: { comment: @review_anime.comment, rating: @review_anime.rating } }
    end

    assert_redirected_to review_anime_url(ReviewAnime.last)
  end

  test "should show review_anime" do
    get review_anime_url(@review_anime)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_anime_url(@review_anime)
    assert_response :success
  end

  test "should update review_anime" do
    patch review_anime_url(@review_anime), params: { review_anime: { comment: @review_anime.comment, rating: @review_anime.rating } }
    assert_redirected_to review_anime_url(@review_anime)
  end

  test "should destroy review_anime" do
    assert_difference('ReviewAnime.count', -1) do
      delete review_anime_url(@review_anime)
    end

    assert_redirected_to review_animes_url
  end
end
