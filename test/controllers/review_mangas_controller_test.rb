require 'test_helper'

class ReviewMangasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review_manga = review_mangas(:one)
  end

  test "should get index" do
    get review_mangas_url
    assert_response :success
  end

  test "should get new" do
    get new_review_manga_url
    assert_response :success
  end

  test "should create review_manga" do
    assert_difference('ReviewManga.count') do
      post review_mangas_url, params: { review_manga: { comment: @review_manga.comment, rating: @review_manga.rating } }
    end

    assert_redirected_to review_manga_url(ReviewManga.last)
  end

  test "should show review_manga" do
    get review_manga_url(@review_manga)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_manga_url(@review_manga)
    assert_response :success
  end

  test "should update review_manga" do
    patch review_manga_url(@review_manga), params: { review_manga: { comment: @review_manga.comment, rating: @review_manga.rating } }
    assert_redirected_to review_manga_url(@review_manga)
  end

  test "should destroy review_manga" do
    assert_difference('ReviewManga.count', -1) do
      delete review_manga_url(@review_manga)
    end

    assert_redirected_to review_mangas_url
  end
end
