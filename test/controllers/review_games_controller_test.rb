require 'test_helper'

class ReviewGamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review_game = review_games(:one)
  end

  test "should get index" do
    get review_games_url
    assert_response :success
  end

  test "should get new" do
    get new_review_game_url
    assert_response :success
  end

  test "should create review_game" do
    assert_difference('ReviewGame.count') do
      post review_games_url, params: { review_game: { comment: @review_game.comment, rating: @review_game.rating } }
    end

    assert_redirected_to review_game_url(ReviewGame.last)
  end

  test "should show review_game" do
    get review_game_url(@review_game)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_game_url(@review_game)
    assert_response :success
  end

  test "should update review_game" do
    patch review_game_url(@review_game), params: { review_game: { comment: @review_game.comment, rating: @review_game.rating } }
    assert_redirected_to review_game_url(@review_game)
  end

  test "should destroy review_game" do
    assert_difference('ReviewGame.count', -1) do
      delete review_game_url(@review_game)
    end

    assert_redirected_to review_games_url
  end
end
