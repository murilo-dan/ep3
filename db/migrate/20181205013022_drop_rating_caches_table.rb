class DropRatingCachesTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :rating_caches
  end

  def down
    create_table :rating_caches
  end
end
