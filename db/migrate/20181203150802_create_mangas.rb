class CreateMangas < ActiveRecord::Migration[5.2]
  def change
    create_table :mangas do |t|
      t.string :title
      t.string :synopsis
      t.string :author

      t.timestamps
    end
  end
end
