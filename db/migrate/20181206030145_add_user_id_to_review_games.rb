class AddUserIdToReviewGames < ActiveRecord::Migration[5.2]
  def change
    add_column :review_games, :user_id, :integer
  end
end
