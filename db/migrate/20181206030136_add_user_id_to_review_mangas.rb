class AddUserIdToReviewMangas < ActiveRecord::Migration[5.2]
  def change
    add_column :review_mangas, :user_id, :integer
  end
end
