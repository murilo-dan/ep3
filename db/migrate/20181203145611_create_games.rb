class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.string :title
      t.string :description
      t.string :rating
      t.string :developer
      t.string :genre
      t.string :publisher
      t.string :release_date
      t.string :platform

      t.timestamps
    end
  end
end
