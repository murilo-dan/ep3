class AddAnimeIdToReviewAnimes < ActiveRecord::Migration[5.2]
  def change
    add_column :review_animes, :anime_id, :integer
  end
end
