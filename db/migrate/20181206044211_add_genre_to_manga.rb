class AddGenreToManga < ActiveRecord::Migration[5.2]
  def change
    add_column :mangas, :genre, :string
  end
end
