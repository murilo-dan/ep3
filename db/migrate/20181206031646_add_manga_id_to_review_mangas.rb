class AddMangaIdToReviewMangas < ActiveRecord::Migration[5.2]
  def change
    add_column :review_mangas, :manga_id, :integer
  end
end
