class AddBookIdToReviewBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :review_books, :book_id, :integer
  end
end
